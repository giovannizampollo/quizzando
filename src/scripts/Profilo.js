import AbsPage from "./AbsPage";

export default class Profilo extends AbsPage {
    constructor(pagedata) {
        super(pagedata);
        pagedata.$el.find("li").remove();
    }
}