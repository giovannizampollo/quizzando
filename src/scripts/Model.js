import Signal from "signals";

export default class Model {

  constructor() {
    if (!Model.instance) {
      this._data = [];
      this.onError = new Signal();
      Model.instance = this;
    }
    return Model.instance;
  }

  login(user,password) {
    Framework7.request({
      url: 'https://gesco.bearzi.it/api/login_check',
      data: {
        "username": user,
        "password": password
      },
      method: "POST",
      contentType: "application/json",
      dataType: "json",
      crossDomain: true,
      success: (data, status, xhr) => {
        console.log("success",data,status);
      },
      error: (xhr, status) => {
        console.log("error",status);
      }
    });
    /*
    Framework7.request.postJSON(
      'https://gesco.bearzi.it/api/login_check',
      {
        "username": user,
        "password": password
      },
      (data, status, xhr) => {
        console.log("success",data,status);
      },
      (xhr, status) => {
        console.log("error",status);
        this.onError.dispatch();
      }
      );
      */
  }

}