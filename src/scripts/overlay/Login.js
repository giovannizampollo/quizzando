import Model from "../model/Model";

export default class Login {
    constructor(loginscreen) {
        this._loginscreen = loginscreen;
        this._model = new Model();
        console.log(this._loginscreen);
        console.log(this._loginscreen.$el.find("#log-in-button"));
        this._loginscreen.$el.find("#log-in-button").on("click", () => this._onClickLogin());
    }

    close() {
        console.log(this._loginscreen);
        this._loginscreen.close();
    }

    _onClickLogin() {
        /** lancio la validazione di Framework7 */
        var app = this._loginscreen.app;
        var form = this._loginscreen.$el.find("form");
        app.input.validateInputs(form);
        /** controllo se il form è valido, se è compilato correttamente */
        var formValid = form[0].checkValidity();
        if (formValid) {
            var formdata = app.form.convertToData(form[0]);
            this._model.login(formdata.username,formdata.password);
        }
    }
}