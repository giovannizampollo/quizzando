import Signal from "signals";

export default class Model {
    constructor() {
        /** questo controllo fa in modo che la classe
         * sia un Singleton, cioè che ha sempre solo un'istanza 
         * anche se viene fatto new in più parti del codice
         */
        if (!Model.instance) {
            this.onUtente = new Signal();
            this.onLogin = new Signal();
            this.onErroreConnessione = new Signal();
            Model.instance = this;
        }
        return Model.instance;
    }

    getUtente() {
        this.onUtente.dispatch({
            type: "onUtente",
            data: this.utente
        });
    }

    login(username, password) {
        Framework7.request({
            url: 'https://gesco.bearzi.it/api/login_check',
            data: {
                username: username,
                password: password
            },
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,
            success: (response) => {
                console.log("success",response);
                var token = response.token;
                this.utente = {
                    token: token
                };
                this.onLogin.dispatch({
                    type: "onLogin",
                    data: this.utente
                });
                this.onUtente.dispatch({
                    type: "onUtente", 
                    data: this.utente
                });
            },
            error: () => {
                console.log("error");
                this.onErroreConnessione.dispatch({type: "onErroreConnessione"});
            }
        });
        /*
        this.utente = {
            username: "utente1",
            avatar: "http://www.asdf.it/a.jpg",
            punteggio: 150,
            livello: 1,
            posizione: 5
        };
        this.onLogin.dispatch({
            type: "onLogin",
            data: this.utente
        });
        this.onUtente.dispatch({
            type: "onUtente", 
            data: this.utente
        });
        */
    }

}