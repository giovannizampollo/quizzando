import AbsPage from "./AbsPage";

export default class Domanda extends AbsPage {
    constructor(pagedata) {
        super(pagedata);
        pagedata.$el.find("li").remove();
    }
}