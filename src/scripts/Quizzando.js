import Home from "./pages/Home";
import Profilo from "./pages/Profilo";
import Risultato from "./pages/Risultato";
import Domanda from "./pages/Domanda";
import Classifica from "./pages/Classifica";
import Categorie from "./pages/Categorie";
import Model from "./model/Model";
import Login from "./overlay/Login";

/**
 * export default permette alla classe
 * di essere importata da altre
 */
export default class Quizzando {
    constructor() {
        console.log("new Quizzando");
        // dichiariamo le variabili di istanza
        this.app = null;
        this.mainView = null;
        this.pages = {};
        // controlliamo se siamo in ambiente cordova o nel browser
        Quizzando.isCordova = (typeof cordova !== "undefined");
        if (Quizzando.isCordova) {
            // attendiamo il deviceready di cordova
            $(document).on("deviceready", () => {
                console.log("deviceready");
                this._init();
            });
        } else {
            // attendiamo l'evento load di window
            $(window).on("load", () => {
                console.log("ready");
                this._init();
            });
        }
    };

    /**
     * inizializza Framework7
     */
    _init() {
        this.app = new Framework7({
            // App root element
            root: '#app',
            // App Name
            name: 'Quizzando 2018',
            // tema forzato su android
            theme: "md",
            // App id
            id: 'it.bearzi.quizzando',
            // Enable swipe panel
            panel: {
                swipe: 'right',
            },
            // comportamento form
            input: {
                scrollIntoViewOnFocus: true,
                scrollIntoViewCentered: true,
            },
            // definiamo delle funzioni collegate ad eventi del framework
            on: {
                /** la pagina è pronta per essere visualizzata */
                pageInit: (pagedata) => {
                    try {
                        console.log('Page initialized ' + pagedata.name);
                        console.log(pagedata);
                        var pageClass = pagedata.route.route.pageClass;
                        var url = pagedata.route.path;
                        console.log("url:" + url);
                        this.pages[url] = new pageClass(pagedata);
                    } catch (error) {
                        console.log(error);
                    }
                },
                /** la pagina sta per essere rimossa */
                pageBeforeRemove: (pagedata) => {
                    try {
                        console.log('Page before remove ' + pagedata.name);
                        console.log(pagedata);
                        var url = pagedata.route.path;
                        console.log("url:" + url);
                        delete this.pages[url];
                    } catch (error) {
                        console.log(error);
                    }
                },
            },
            // Add default routes
            routes: [{
                    path: '/categorie/',
                    url: 'public/categorie.html',
                    pageClass: Categorie
                },
                {
                    path: '/classifica/',
                    url: 'public/classifica.html',
                    pageClass: Classifica
                },
                {
                    path: '/domanda/',
                    url: 'public/domanda.html',
                    pageClass: Domanda
                },
                {
                    path: '/risultato/',
                    url: 'public/risultato.html',
                    pageClass: Risultato
                },
                {
                    path: '/profilo/',
                    url: 'public/profilo.html',
                    pageClass: Profilo
                },
                {
                    path: '/home/',
                    url: 'public/home.html',
                    pageClass: Home
                },
            ],
            // ... other parameters
        });
        // inizializzare la view principale
        this.mainView = this.app.views.create('.view-main');
        this._initModel();
    }

    _initModel() {
        this.model = new Model();
        // controlliamo se l'utente è loggato
        this.model.onUtente.add(this._onUtenteSignal, this);
        this.model.getUtente();
    }

    _onUtenteSignal(signal_object) {
        console.log(signal_object);
        if (signal_object.data) {
            this.mainView.router.navigate("/home/");
            //this.app.loginScreen.close();
            //this.login._loginscreen.close();
            this.login.close();
        } else {
            if (!this.login) {
                var loginscreen = this.app.loginScreen.open(".login-screen");
                this.login = new Login(loginscreen);
            }
            // TODO: se esiste già andrebbe riaperta
        }
    }
}