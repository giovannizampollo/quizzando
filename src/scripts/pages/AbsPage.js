/**
 * classe astratta, non viene utilizzata con in comando new
 * ma viene estesa da altre classi
 * contiene le funzioni condivise da tutte le pagine
 */
export default class AbsPage {
    constructor(pagedata) {
        this._pagedata = pagedata;
        this._init();
    }
    _render() {

    }
    _init() {
        console.log("init");
    }
    destroy() {
        delete this._pagedata;
    }
}