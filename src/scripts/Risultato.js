import AbsPage from "./AbsPage";

export default class Risultato extends AbsPage {
    constructor(pagedata) {
        super(pagedata);
        pagedata.$el.find("li").remove();
    }
}